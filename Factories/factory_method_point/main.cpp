/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <cmath>
#include <iostream>

class Point {
public:
    friend std::ostream& operator<<(std::ostream& os, Point const& point)
    {
        os << "(x:" << point.x << ", y:" << point.y << ')';
        return os;
    }

    class Factory {
    public:
        Factory() = delete;

        static Point newCartesianPoint(double x, double y) { return Point { x, y }; }

        static Point newPolar(double r, double theta)
        {
            return Point { r * std::cos(theta), r * std::sin(theta) };
        }
    };

private:
    Point(double x_, double y_)
        : x(x_)
        , y(y_)
    {
    }

    double x, y;
};

int main()
{
    auto p = Point::Factory::newPolar(5.0, M_PI_4);
    std::cout << "New point: " << p << std::endl;
    return 0;
}
