/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <Person.hpp>
#include <iostream>

/**
 * @brief The Renderer struct
 */
struct Renderer {
    virtual void render_circle(float x, float y, float radius) = 0;

    virtual ~Renderer() = default;
};

/**
 * @brief The VectorRenderer struct
 */
struct VectorRenderer : Renderer {
    void render_circle(float x, float y, float radius) override
    {
        std::cout << "Drawing a vector circle of radius " << radius << " in x:" << x
                  << " y:" << y << std::endl;
    }
};

/**
 * @brief The RasterRenderer struct
 */
struct RasterRenderer : Renderer {
    void render_circle(float x, float y, float radius) override
    {
        std::cout << "Rasterizing a circle of radius " << radius << " in x:" << x
                  << " y:" << y << std::endl;
    }
};

/**
 * @brief The Shape struct
 */
struct Shape {
protected:
    Renderer& renderer;

    Shape(Renderer& r)
        : renderer(r)
    {
    }

public:
    virtual void draw() = 0;

    virtual void resize(float const factor) = 0;

    virtual ~Shape() = default;
};

/**
 * @brief The Circle struct
 */
struct Circle : public Shape {
    float x, y, r;

    Circle(Renderer& renderer, float x, float y, float radius)
        : Shape(renderer)
        , x(x)
        , y(y)
        , r(radius)
    {
    }

public:
    void draw() override { renderer.render_circle(x, y, r); }
    void resize(const float factor) override { r *= factor; }
};

int main()
{
    // PIMPL test
    Person p { "Sławek" };
    p.greet();

    // Renderer test
    RasterRenderer rr;
    Circle c1(rr, 1.f, 1.f, 3.14f);

    c1.draw();
}
