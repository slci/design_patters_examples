/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <vector>

// CRTP Neuron
template <typename Self>
struct Connectable {
    template <typename T>
    void connect_to(T& other)
    {
        for (auto& from : static_cast<Self&>(*this)) {
            for (auto& to : other) {
                from.outputs.push_back(&to);
                to.inputs.push_back(&from);
            }
        }
    }
};

struct Neuron : Connectable<Neuron> {
    unsigned id;
    std::vector<Neuron*> inputs;
    std::vector<Neuron*> outputs;

    Neuron()
    {
        static unsigned id { 0 };
        this->id = id++;
    }

    friend std::ostream& operator<<(std::ostream& os, const Neuron& neuron)
    {
        for (auto n : neuron.inputs) {
            os << n->id << "\t-->\t[" << neuron.id << "]\n";
        }
        for (auto n : neuron.outputs) {
            os << "[" << neuron.id << "]\t-->\t" << n->id << std::endl;
        }

        return os;
    }

    auto begin() { return this; }

    auto end() { return this + 1; }
};

struct NeuronLayer : Connectable<NeuronLayer> {
    std::vector<Neuron> neurons;

    NeuronLayer(unsigned count)
    {
        while (count-- > 0) {
            neurons.emplace_back(Neuron {});
        }
    }

    friend std::ostream& operator<<(std::ostream& os, const NeuronLayer& layer)
    {
        for (auto n : layer.neurons) {
            os << n;
        }
        return os;
    }

    auto begin() { return neurons.begin(); }

    auto end() { return neurons.end(); }
};

int main()
{
    Neuron n1, n2;
    NeuronLayer l1 { 2 }, l2 { 3 };
    n1.connect_to(l1);
    l1.connect_to(l2);
    l2.connect_to(n2);

    std::cout << l1 << std::endl;
    std::cout << l2 << std::endl;

    return 0;
}
