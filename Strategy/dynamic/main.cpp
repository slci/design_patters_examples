/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <sstream>
#include <memory>
#include <string>
#include <vector>

enum class OutputFormat
{
    markdown,
    html
};

struct ListStrategy
{
    virtual ~ListStrategy() = default;

    virtual void start(std::ostringstream &) {}
    virtual void end(std::ostringstream &) {}
    virtual void add_list_item(std::ostringstream &, const std::string &) {}
};

struct MarkdownStrategy : public ListStrategy
{
    void add_list_item(std::ostringstream &oss, const std::string &item) override
    {
        oss << "* " << item << std::endl;
    }
};

struct HtmlStrategy : public ListStrategy
{
    void start(std::ostringstream &oss) override
    {
        oss << "<ul>\n";
    }

    void end(std::ostringstream &oss) override
    {
        oss << "</ul>\n";
    }

    void add_list_item(std::ostringstream &oss, const std::string &item) override
    {
        oss << "  <li>" << item << "</li>\n";
    }
};

struct TextProcessor
{
    TextProcessor() : list_strategy(std::make_unique<MarkdownStrategy>()) {}

    void append(std::vector<std::string> items)
    {
        list_strategy->start(oss);

        for (auto item : items)
        {
            list_strategy->add_list_item(oss, std::move(item));
        }

        list_strategy->end(oss);
    }

    void clear()
    {
        oss.str("");
        oss.clear();
    }

    void set_output_format(const OutputFormat format)
    {
        switch (format)
        {
        case OutputFormat::markdown:
            list_strategy = std::make_unique<MarkdownStrategy>();
            break;
        case OutputFormat::html:
            list_strategy = std::make_unique<HtmlStrategy>();
            break;
        }
    }

    void print()
    {
        std::cout << oss.str() << std::endl;
    }

    std::ostringstream oss;
    std::unique_ptr<ListStrategy> list_strategy{nullptr};
};

int main()
{
    auto text_processor = TextProcessor{};

    text_processor.set_output_format(OutputFormat::markdown);
    text_processor.append({"foo", "bar", "baz"});
    text_processor.print();

    text_processor.clear();

    text_processor.set_output_format(OutputFormat::html);
    text_processor.append({"foo", "bar", "baz"});
    text_processor.print();
}