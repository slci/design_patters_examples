/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef BOOSTFLYWEIGHT_HPP
#define BOOSTFLYWEIGHT_HPP
#include <boost/flyweight.hpp>
#include <string>

using namespace std;

namespace boost_flyweigth {

struct User {
    boost::flyweight<string> first_name, last_name;

    User(string const& f_name, string const& l_name)
        : first_name(f_name)
        , last_name(l_name)
    {
    }

    string const& firstName() const { return first_name.get(); }

    string const& lastName() const { return last_name.get(); }

    friend ostream& operator<<(ostream& os, const User& user)
    {
        os << "first_name[" << user.first_name.get_key()
           << "]: " << user.firstName() << " last_name[" << user.last_name.get_key()
           << "]: " << user.lastName();
        return os;
    }
};

} // namespace boost_flyweigth

#endif // BOOSTFLYWEIGHT_HPP
