/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "BoostFlyweight.hpp"
#include "HandMadeFlyweigth.hpp"
#include "TextFormatting.hpp"

int main()
{
    // Hand made flyweigth test
    hand_made::User u1 { "John", "Doe" };
    hand_made::User u2 { "John", "Smith" };

    assert(&u1.firstName() == &u2.firstName());
    assert(&u1.lastName() != &u2.lastName());

    std::cout << u1 << std::endl
              << u2 << std::endl;

    // Boost flyweigth test
    boost_flyweigth::User u3 { "Marta", "Głuchowska" };
    boost_flyweigth::User u4 { "Marta", "Dębska" };

    assert(&u3.firstName() == &u4.firstName());
    assert(&u3.lastName() != &u4.lastName());

    std::cout << u3 << std::endl
              << u4 << std::endl;

    text_fmt::BetterFormattedText text { "SomeCamelCaseText" };

    std::cout << text << "\n";
    text.getRange(4, 8).capitalize = true;
    std::cout << text << "\n";
}
