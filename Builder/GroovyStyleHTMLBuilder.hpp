/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef GROOVYSTYLEHTMLBUILDER_HPP
#define GROOVYSTYLEHTMLBUILDER_HPP

#include <sstream>
#include <utility>

class Tag {
public:
    Tag(std::string const& name, std::string const& text)
        : name(name)
        , text(text)
    {
    }

    Tag(std::string const& name, std::vector<Tag> const& children)
        : name(name)
        , children(children)
    {
    }

    friend std::ostream& operator<<(std::ostream& os, Tag const& tag)
    {
        os << "<" << tag.name;

        for (auto const& att : tag.attributes) {
            os << " " << att.first << "=\"" << att.second << "\"";
        }

        if (!tag.children.empty() && !tag.text.empty()) {
            os << "/>" << std::endl;
        } else {
            os << ">" << std::endl;

            if (!tag.text.empty()) {
                os << tag.text << std::endl;
            }

            for (auto const& child : tag.children) {
                os << child;
            }

            os << "</" << tag.name << ">" << std::endl;
        }

        return os;
    }

protected:
    std::string name;
    std::string text;
    std::vector<std::pair<std::string, std::string>> attributes;

    std::vector<Tag> children;
};

// Paragraph tag
class P : public Tag {
public:
    P(std::string const& text)
        : Tag("p", text)
    {
    }

    P(std::initializer_list<Tag> children)
        : Tag("p", children)
    {
    }
};

// Image tag
class Img : public Tag {
public:
    Img(std::string const& url)
        : Tag("img", "")
    {
        attributes.emplace_back(std::make_pair("src", url));
    }
};

#endif // GROOVYSTYLEHTMLBUILDER_HPP
