/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <memory>
#include <vector>

struct Memento {
    const int balance;

    explicit Memento(const int b)
        : balance(b)
    {
    }
};

struct BankAccount {
    int ballance { 0 };

    std::vector<std::shared_ptr<Memento>> changes;
    std::vector<std::shared_ptr<Memento>>::iterator current;

    explicit BankAccount(const int init_ballance)
        : ballance(init_ballance)
    {
        std::cout << "New avccount created, initial ballance " << ballance << "$\n";
        changes.emplace_back(std::make_shared<Memento>(ballance));

        current = changes.begin();
    }

    std::shared_ptr<Memento> deposit(const int amount)
    {
        ballance += amount;
        std::cout << amount << "$ goes to account, ballance: " << ballance << "$\n";

        auto m = std::make_shared<Memento>(ballance);
        changes.push_back(m);
        current = changes.end() - 1;
        return m;
    }

    void restore(const std::shared_ptr<Memento>& m)
    {
        if (m) {
            ballance = m->balance;
            changes.push_back(m);
            current = changes.end() - 1;
        }
        std::cout << "Restoring previous ballance: " << ballance << "$\n";
    }

    std::shared_ptr<Memento> undo()
    {
        if (current > changes.begin()) {
            --current;
            auto m = *current;
            ballance = m->balance;

            std::cout << "Undoing previous action: " << ballance << "$\n";
            return m;
        } else {
            return {};
        }
    }

    std::shared_ptr<Memento> redo()
    {
        if (current < changes.end() - 1) {
            ++current;
            auto m = *current;
            ballance = m->balance;

            std::cout << "Redoing previous action: " << ballance << "$\n";
            return m;
        } else {
            return {};
        }
    }
};

int main()
{
    BankAccount ba { 100 };

    (void)ba.deposit(50); // 150
    (void)ba.deposit(25); // 175

    ba.undo();
    ba.undo();
    ba.undo();
    ba.redo();
    ba.redo();
    ba.redo();
}