/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>

class Rectangle {
public:
    explicit Rectangle(int const w, int const h)
        : width_(w)
        , height_(h)
    {
    }

    virtual ~Rectangle() = default;

    int area() const { return width_ * height_; }

    int width() const { return width_; }

    int height() const { return height_; }

    virtual void setWidht(int const w) { width_ = w; }

    virtual void setHeight(int const h) { height_ = h; }

protected:
    int width_;
    int height_;
};

struct Square : public Rectangle {
    explicit Square(int const size)
        : Rectangle(size, size)
    {
    }

    void setWidht(int const w) override
    {
        width_ = w;
        height_ = w;
    }

    void setHeight(int const h) override
    {
        height_ = h;
        width_ = h;
    }
};

void process(Rectangle& r)
{
    int w = r.width();
    r.setHeight(10);

    std::cout << "Expected area: " << (w * 10) << " got: " << r.area()
              << std::endl;
}

int main()
{
    auto r = Rectangle { 15, 13 };
    process(r);

    auto s = Square { 5 };
    process(s);
}
