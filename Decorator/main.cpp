/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "Dynamic.hpp"
#include "Functional.hpp"
#include "Static.hpp"

using namespace std;

int main()
{
    // Dynamic decorators test
    auto c = std::make_unique<Dynamic::Circle>(123.0f);
    auto rc = std::make_unique<Dynamic::ColoredShape>(std::move(c), "Red");
    auto trc = std::make_unique<Dynamic::TransparentShape>(std::move(rc), 20);

    std::cout << trc->str() << std::endl;
    //   trc->resize(2.0f); Can't! The Circle API is lost when decorated :(

    // Static decorators test
    auto trc2 = Static::TransparentShape<Static::ColoredShape<Static::Circle>>(
        30, "Purple", 3.1415f);

    std::cout << trc2.str() << std::endl;

    // We can access the full Circle API! :D
    trc2.resize(2.0f);
    std::cout << trc2.str() << std::endl;

    // Functional decorator test
    auto f = Logger { [] { cout << "  some work..\n"; }, "Decorated function" };
    f();
}
