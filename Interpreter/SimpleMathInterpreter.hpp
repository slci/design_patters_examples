/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MATHLEXER_HPP
#define MATHLEXER_HPP

#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

#include <boost/lexical_cast.hpp>

namespace simple_interpreter {

struct Token {
    enum Type {
        integer,
        plus,
        minus,
        lparen,
        rparen
    } type;

    std::string text;

    Token(const Type t, const std::string& str)
        : type(t)
        , text(str)
    {
    }

    friend std::ostream& operator<<(std::ostream& os, const Token& token)
    {
        os << "`" << token.text << "`";
        return os;
    }
};

std::vector<Token>
lex(const std::string text)
{
    std::vector<Token> tokens;
    for (size_t charIdx = 0; charIdx < text.size(); ++charIdx) {
        switch (text[charIdx]) {
        case '+':
            tokens.emplace_back(Token::plus, "+");
            break;
        case '-':
            tokens.emplace_back(Token::minus, "-");
            break;
        case '(':
            tokens.emplace_back(Token::lparen, "(");
            break;
        case ')':
            tokens.emplace_back(Token::rparen, ")");
            break;
        default:
            if (std::isdigit(text[charIdx])) {
                std::ostringstream buffer;
                buffer << text[charIdx];
                for (size_t i = charIdx + 1; i < text.size(); ++i) {
                    if (std::isdigit(text[i])) {
                        buffer << text[i];
                        ++charIdx;
                    } else {
                        break;
                    }
                }

                tokens.emplace_back(Token::integer, buffer.str());
            } else {
                throw std::runtime_error { std::string("error: Unexpected token: `") + text[charIdx] + '`' };
            }
        }
    }

    return tokens;
}

struct Element {
    virtual int eval() const = 0;

    virtual ~Element() = default;
};

struct Integer : Element {
    int value;

    Integer(const int newVal)
        : value(newVal)
    {
    }

    int eval() const override { return value; }
};

struct BinaryOperation : Element {
    enum Type {
        addition,
        subtraction
    } type;

    std::shared_ptr<Element> lhs, rhs;

    int eval() const override
    {
        switch (type) {
        case addition:
            return lhs->eval() + rhs->eval();
        case subtraction:
            return lhs->eval() - rhs->eval();
        }
        return 0;
    }
};

std::shared_ptr<Element>
parse(const std::vector<Token>& tokens)
{
    auto rootOp = std::make_shared<BinaryOperation>();

    bool haveLhs { false };

    for (size_t tokenIdx = 0; tokenIdx < tokens.size(); ++tokenIdx) {
        const auto& token = tokens[tokenIdx];
        switch (token.type) {
        case Token::integer: {
            const int value = boost::lexical_cast<int>(token.text);
            auto integer = std::make_shared<Integer>(value);
            if (!haveLhs) {
                rootOp->lhs = integer;
                haveLhs = true;
            } else {
                rootOp->rhs = integer;
            }
            break;
        }
        case Token::minus: {
            rootOp->type = BinaryOperation::subtraction;
            break;
        }
        case Token::plus: {
            rootOp->type = BinaryOperation::addition;
            break;
        }
        case Token::lparen: {
            bool rparenFound { false };
            size_t rparenIdx = tokenIdx + 1;
            for (; rparenIdx < tokens.size(); ++rparenIdx) {
                if (tokens[rparenIdx].type == Token::rparen) {
                    rparenFound = true;
                    break;
                } else if (tokens[rparenIdx].type == Token::lparen) {
                    throw std::runtime_error {
                        "error: Nested parens not supported yet"
                    };
                }
            }
            if (rparenFound) {
                std::vector<Token> subExpression { tokens.begin() + tokenIdx + 1,
                    tokens.begin() + rparenIdx };
                auto element = parse(subExpression);
                if (!haveLhs) {
                    rootOp->lhs = element;
                    haveLhs = true;
                } else {
                    rootOp->rhs = element;
                }
                tokenIdx = rparenIdx;
            } else {
                throw std::runtime_error { "error: Missing `)`" };
            }
            break;
        }
        case Token::rparen: {
            throw std::runtime_error { "error: `)` should be ignored" };
            break;
        }
        }
    }

    return rootOp;
}

} // namespace simple_interpreter

#endif // MATHLEXER_HPP
