/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MATHIWITHVARIABLESINTERPRETER_HPP
#define MATHIWITHVARIABLESINTERPRETER_HPP

#include <cstdint>
#include <iostream>
#include <map>
#include <memory>
#include <optional>
#include <sstream>
#include <vector>

struct ExpressionProcessor {
    std::map<char, int> variables;

    enum class Operation {
        plus,
        minus,
        invalid
    };

    int calculate(const std::string& text)
    {
        std::optional<int> lhs;
        Operation op { Operation::invalid };

        for (size_t charIdx = 0; charIdx < text.size(); ++charIdx) {
            switch (text[charIdx]) {
            case '+': {
                if (!lhs) {
                    throw std::runtime_error { "error: + operator needs lhs" };
                } else {
                    op = Operation::plus;
                }
                break;
            }
            case '-': {
                if (!lhs) {
                    throw std::runtime_error { "error: - operator needs lhs" };
                } else {
                    op = Operation::minus;
                }
                break;
            }
            default: {
                if (std::isdigit(text[charIdx])) {
                    std::ostringstream buffer;
                    buffer << text[charIdx];
                    for (size_t i = charIdx + 1; i < text.size(); ++i) {
                        if (std::isdigit(text[i])) {
                            buffer << text[i];
                            ++charIdx;
                        } else {
                            break;
                        }
                    }
                    if (!lhs) {
                        lhs = std::stoi(buffer.str());
                    } else {
                        switch (op) {
                        case Operation::plus:
                            lhs = lhs.value() + std::stoi(buffer.str());
                            break;
                        case Operation::minus:
                            lhs = lhs.value() - std::stoi(buffer.str());
                            break;
                        default:
                            throw std::runtime_error { "error: invalid operation" };
                        }
                    }
                } else if (std::isalpha(text[charIdx])) {
                    if ((charIdx + 1) < text.size() && std::isalpha(text[charIdx + 1])) {
                        throw std::runtime_error {
                            "error: multi-charater variables unsupported"
                        };
                    } else {
                        if (!lhs) {
                            lhs = variables.at(text[charIdx]);
                        } else {
                            switch (op) {
                            case Operation::plus:
                                lhs = lhs.value() + variables.at(text[charIdx]);
                                break;
                            case Operation::minus:
                                lhs = lhs.value() - variables.at(text[charIdx]);
                                break;
                            default:
                                throw std::runtime_error { "error: invalid operation" };
                            }
                        }
                    }
                } else {
                    throw std::runtime_error { std::string(
                                                   "error: Unexpected character: `")
                        + text[charIdx] + '`' };
                }
            }
            }
        }

        if (lhs) {
            return lhs.value();
        } else {
            return 0;
        }
    }
};

#endif // MATHIWITHVARIABLESINTERPRETER_HPP
