/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "CommunicationProxy.hpp"
#include "PropertyProxy.hpp"
#include "VirtualProxy.hpp"

struct Creature {
    Property<int> strength;
    Property<int> agility;
};

using namespace std;

int main()
{
    // Property Proxy test
    Creature c;

    c.agility = 123;
    c.strength = 321;

    std::cout << c.agility << ' ' << c.strength << std::endl;

    // Virtual Proxy test
    auto img1 = virtual_proxy::LazyBitmap { "a picture 1" };
    auto img2 = virtual_proxy::LazyBitmap { "a picture 2" };
    img2.draw();
    img1.draw();
    img1.draw();

    // Communication Proxy test
    communication_proxy::RemoteService rs("some service");

    for (size_t i = 0; i < 3; ++i) {
        communication_proxy::tryit(rs);
    }
}
