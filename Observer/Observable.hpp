/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef OBSERVABLE_HPP
#define OBSERVABLE_HPP

#include <algorithm>
#include <vector>

#include "Observer.hpp"

template <typename T>
class Observable {
private:
    std::vector<Observer<T>*> observers;

public:
    Observable() = default;

    void notify(T& source, const std::string& field_name)
    {
        for (const auto& obs : observers) {
            obs->field_changed(source, field_name);
        }
    }

    void subscribe(Observer<T>& observer)
    {
        observers.push_back(&observer);
    }

    void unsubscribe(Observer<T>& observer)
    {
        observers.erase(
            std::remove(observers.begin(), observers.end(), &observer),
            observers.end());
    }
};

#endif /* OBSERVABLE_HPP */
