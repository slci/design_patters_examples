/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <atomic>
#include <boost/signals2/signal.hpp>
#include <iostream>

#include "Observer.hpp"
#include "Person.hpp"

class ConsolePersonObserver : public Observer<Person> {

    void field_changed(Person& source, const std::string& field_name) override
    {
        std::cout << "Person's " << field_name << " has changed to: ";
        if (field_name == "age") {
            std::cout << source.get_age() << std::endl;
        } else if (field_name == "can_vote") {
            std::cout << std::boolalpha << source.get_can_vote() << std::endl;
        } else {
            throw std::logic_error{ "source doesn't support get_age property" };
        }
    }
};

using boost::signals2::signal;

template <typename T>
struct Observable2 {
    signal<void(const T&, const std::string& field_changed)> field_changed;
};

class Person2 : public Observable2<Person2> {
    int age;

public:
    Person2(const int age_)
        : age(age_)
    {
    }

    int get_age() const
    {
        return age;
    }

    void set_age(const int age_)
    {
        if (age != age_) {
            age = age_;
            field_changed(*this, "age");
        }
    }
};

struct TrafficAdministration : Observer<Person> {
    void field_changed(Person& source, const std::string& field_name) override
    {
        if (field_name == "age") {
            if (source.get_age() < 17) {
                std::cout << "Whoa there, you're not old enough to drive!\n";
            } else {
                std::cout << "Ok, we no longer care.\n";
                source.unsubscribe(*this); // Called from within Observable::notify() !!!!
            }
        }
    }

    static std::atomic<int> nr;
};

std::atomic<int> TrafficAdministration::nr = 1;

int main()
{
    // Custom observer test
    {
        Person p{ 15 };
        ConsolePersonObserver cpo;
        TrafficAdministration ta;

        p.subscribe(cpo);
        p.subscribe(ta);

        p.set_age(16);
        p.set_age(17);
        p.set_age(18);
        p.set_age(19);
        p.unsubscribe(cpo);
        p.set_age(20);
        p.set_age(21);
    }

    // Observer with boost::signals2 test
    {
        Person2 p{ 47 };

        auto connection = p.field_changed.connect([](const Person2& source, const std::string& field_name) {
            std::cout << "Person2's " << field_name << " has changed to: ";
            if (field_name == "age") {
                std::cout << source.get_age() << std::endl;
            } else {
                throw std::logic_error{ "source doesn't support get_age property" };
            }
        });

        p.set_age(48);
        p.set_age(49);
        p.set_age(50);
        connection.disconnect();
        p.set_age(51);
        p.set_age(52);
    }
}