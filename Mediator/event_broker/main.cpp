/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <boost/signals2.hpp>
#include <iostream>

using boost::signals2::signal;

struct EventData {
    virtual ~EventData() = default;

    virtual void print() const = 0;
};

struct PlayerScoredData : public EventData {
    const std::string player_name;
    int goals_scored_so_far;

    PlayerScoredData(const std::string name, const int goals)
        : player_name(name)
        , goals_scored_so_far(goals)
    {
    }

    void print() const override
    {
        std::cout << "Player: " << player_name
                  << " has scoreed! (goals: " << goals_scored_so_far << ")\n";
    }
};

struct Game {
    signal<void(EventData*)> event;
};

struct Player {
    const std::string player_name;
    int goals_scored { 0 };

    Game& game;

    Player(const std::string name, Game& g)
        : player_name(name)
        , game(g)
    {
    }

    void score()
    {
        goals_scored++;
        PlayerScoredData ps { player_name, goals_scored };
        game.event(&ps);
    }
};

struct Coach {
    Game& game;

    Coach(Game& g)
        : game(g)
    {
        game.event.connect([](EventData* e) {
            auto ps = dynamic_cast<PlayerScoredData*>(e);
            if (ps) {
                std::cout << "Coach says: well done " << ps->player_name << "!"
                          << std::endl;
            }
        });
    }
};

int main()
{
    Game game; // event broker

    Player messy { "Messy", game };
    Coach coach { game };

    messy.score();
    messy.score();
    messy.score();
}
